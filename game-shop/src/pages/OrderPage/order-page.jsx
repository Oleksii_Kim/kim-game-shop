import './order-page.scss';
import {useSelector} from "react-redux";
import OrderItem from "../../components/order-item/order-item";
import CalcTotalPrice from "../../components/utils";
import ValidationCart from "../../components/validation-cart/validation-cart";


const OrderPage = (props) => {
    const items = useSelector(state => state.cart.itemsInCart);

    if (items.length < 1) {
        return <h1>Ваша корзина пуста.</h1>;
    }


    return (
        <div className="order-page">
            <div className="order-page__left">
                {items.map(game => <OrderItem game={game}/>)}
            </div>
            <div className="order-page__right">
              <div className="order-page__total-price">
                  <span>
                      {items.length} товаров на сумму {CalcTotalPrice(items)} грн.
                  </span>
              </div>
            </div>
            <div>
                <ValidationCart/>
            </div>
        </div>
    );
};


export default OrderPage;