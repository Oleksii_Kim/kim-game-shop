import {Link} from "react-router-dom";
import CartBlock from "../cart-block/cart-block";
import './header.scss'

const Header = ()=>{

    return(

        <div className='header'>
            <div className='wrapper'>
                    <Link to="/home" className='header_store-title'>KIM GAMES</Link>
            </div>
            <div className='wrapper header_cart-btn-wrapper'>
              <CartBlock/>
            </div>
        </div>
    )
}


export default Header