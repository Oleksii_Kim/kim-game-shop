import './game-buy.scss';
import {useDispatch, useSelector} from "react-redux";
import Button from "../button/button";
import {deleteItemFromCart, setItemInCart} from "../../redux/cart/reducer";


const GameBuy = (props) => {
    const dispatch = useDispatch()
    const items = useSelector(state=> state.cart.itemsInCart)
    const isItemInCart = items.some(item => item.id === props.game.id)

    const handleClick = (e) => {
        e.stopPropagation()
        if (isItemInCart){
            dispatch(deleteItemFromCart(props.game.id))
        } else {
            dispatch(setItemInCart(props.game))
        }
    };
    return (
        <div className="game-buy">
            <span className="game-buy_price">{props.game.price} грн.</span>
            <Button
                type={isItemInCart? 'secondary' : "primary"}
                onClick={handleClick}>
                {isItemInCart ? 'Убрать из корзины' : 'В корзину'}
            </Button>
        </div>
    );
};

export default GameBuy;