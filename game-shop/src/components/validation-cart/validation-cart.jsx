import './validation-cart.scss'

import React from 'react';
import {Formik, Form, ErrorMessage} from 'formik';
import * as yup from 'yup';
import {useDispatch} from "react-redux";
import {clearAll} from "../../redux/cart/reducer";


const CustomErrorMessage = ({children}) => {
    return (<div style={{color: 'red'}}><span>{children}</span></div>);
};

const validationSchema = yup.object().shape({
    firstName: yup.string().typeError('Должно быть строкой').min(4, 'Too Short!').max(12, 'Too Long!').required('Обязательно'),
    lastName: yup.string().typeError('Должно быть строкой').min(2, 'Too Short!').max(12, 'Too Long!').required('Обязательно'),
    age: yup.number().min(16, 'Incorrect age').max(100, 'Incorrect age!').required('Обязательно'),
    address: yup.string().min(5, 'Too Short!').max(30, 'Too Long!').required('Обязательно'),
    phone: yup.number().typeError('Must be number').required('Обязательно'),
});
const ValidationCart = () => {
    const dispatch = useDispatch();
    const handleSubmit = () => {
        dispatch(clearAll())
    };

    return (
        <>
            <h1>Потверждение заказа</h1>
            <Formik
                initialValues={{firstName: '', lastName: '', age: '', address: '', phone: ''}}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >
                {({values,
                      handleChange, handleBlur, isValid, handleSubmit, dirty}) => (
                    <Form>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`firstName`}>Имя</label><br/>
                            <input type={"text"} name={`firstName`} onChange={handleChange} onBlur={handleBlur} value={values.firstName}/>
                            <ErrorMessage name="firstName" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`lastName`}>Фамилие</label><br/>
                            <input type={"text"} name={`lastName`} onChange={handleChange} onBlur={handleBlur} value={values.lastName}/>
                            <ErrorMessage name="lastName" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`age`}>Возраст</label><br/>
                            <input type={"number"} name={`age`} onChange={handleChange} onBlur={handleBlur} value={values.age}/>
                            <ErrorMessage name="age" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`address`}>Адрес</label><br/>
                            <input type={"text"} name={`address`} onChange={handleChange} onBlur={handleBlur} value={values.address}/>
                            <ErrorMessage name="address" component={CustomErrorMessage}/>
                        </div>
                        <div className="mb-3">
                            <label style={{fontWeight: 'bold'}} htmlFor={`phone`}>Номер телфона</label><br/>
                            <input type={"text"} name={`phone`} onChange={handleChange} onBlur={handleBlur} value={values.phone}/>
                            <ErrorMessage name="phone" component={CustomErrorMessage}/>
                        </div>

                        <div>
                            <button disabled={!isValid && !dirty} onClick={handleSubmit} type="submit">Оформить</button>
                        </div>

                    </Form>
                )}
            </Formik>
        </>
    );
};

export default ValidationCart;