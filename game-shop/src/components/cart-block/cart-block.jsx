import './cart-block.scss';
import {BiCartAlt} from 'react-icons/bi';
import {useSelector} from "react-redux";
import CalcTotalPrice from "../utils";
import CartMenu from "../cart-menu/cart-menu";
import {useCallback, useState} from "react";
import ItemsInCart from "../items-in-cart/items-in-cart";
import {useNavigate} from "react-router";

const CartBlock = () => {
    const [isCartMenuVisible, setIsCartMenuVisible] = useState(false);
    const items = useSelector(state => state.cart.itemsInCart);
    const totalPrice = CalcTotalPrice(items);
    const navigate = useNavigate();
    const handleClick = useCallback(() => {
        setIsCartMenuVisible(false);
        navigate(`/order`);
    }, [navigate]);
    return (
        <div className="cart-block">
            <ItemsInCart quantity={items.length}/>
            <BiCartAlt onClick={() => setIsCartMenuVisible(!isCartMenuVisible)} size={25} className="cart-block_icon"/>
            {totalPrice > 0 ? <span className="cart-block_total-price">{totalPrice} грн.</span> : null}
            {isCartMenuVisible && <CartMenu items={items} onClick={handleClick}/>}
        </div>
    );
};


export default CartBlock;