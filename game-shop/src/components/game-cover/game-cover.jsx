import './game-cover.scss';

const GameCover = (props) => {
    return (
        <div className="game-cover" style={{backgroundImage:`url(${props.image}`}}>

        </div>
    );
};
export default GameCover;