import './cart-item.scss'


const CartItem=({title,price,id,image})=>{
    return(
        <div className='cart-item'>
            <span>{title}</span>
            <div className="cart-item__price">
                {price} грн.
            </div>
        </div>
    )
}
export default CartItem