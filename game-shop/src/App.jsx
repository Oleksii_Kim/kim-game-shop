import React from "react";
import './App.scss';
import {Provider} from "react-redux";
import {Route, Routes} from 'react-router-dom';
import Home from "./pages/Home/home";
import GamePage from './pages/Game/game';
import Header from "./components/header/header";
import OrderPage from "./pages/OrderPage/order-page";
import store from './redux';





function App() {

    return (
        <Provider store={store}>
            <div className="App">
                <Header/>
                <Routes>
                    <Route path="/home" element={<Home/>}/>
                    <Route path="/app/:title" element={<GamePage/>}/>
                    <Route path="/order" element={<OrderPage/>}/>
                </Routes>
            </div>
        </Provider>
    );
}

export default App;
